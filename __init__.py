import os
import re
import glob
import time
import shutil
import platform
import subprocess
import tempfile
import gazu
import json
from kabaret import flow
from kabaret.app.ui.gui.icons import gui as _
from libreflow.utils.flow import keywords_from_format
from libreflow.utils.flow.values import MultiOSParam
from libreflow.baseflow.file import GenericRunAction
from kabaret.app import resources
from .utils.scripts import blender as _


def copytree_no_metadata(src, dst):
    '''
    Recursively copy an entire directory tree rooted at `src`
    to a directory named `dst`, without copying metadata.
    '''
    if not os.path.isdir(dst):
        os.mkdir(dst)
    
    for f in os.scandir(src):
        if f.is_dir():
            copytree_no_metadata(f.path, os.path.join(dst, f.name))
        else:
            shutil.copyfile(f.path, os.path.join(dst, f.name))


def safe_format(string, keywords):
    string_kwords = keywords_from_format(string)
    for kw in string_kwords:
        if kw in keywords:
            string = string.replace('{'+kw+'}', keywords[kw])
    
    return string


class SafeDict(dict):

    def __missing__(self, key):
        return '<' + key + '>'


class FilePresetItem(flow.Object):

    display_name = flow.Param()
    path_regex = flow.Param()
    target_task = flow.Param()
    target = flow.Param()
    comment = flow.Param()
    check_exists = flow.BoolParam(False)
    warning_exists = flow.BoolParam(False)

    _film = flow.Parent(3)

    @staticmethod
    def validate_path(path):
        return os.path.isfile(path)

    def find_matches(self, root_folder, **keywords):
        regex = self.path_regex.get()

        if not root_folder or not os.path.exists(root_folder) or regex is None:
            return
        
        regex = safe_format(regex, keywords)
        matches = []
        
        for path in glob.iglob(root_folder+'/**/*', recursive=True):
            if not self.validate_path(path):
                continue

            relpath = path.replace('\\', '/').replace(root_folder.replace('\\', '/') + '/', '')
            m = re.match(regex, relpath)

            if m is None:
                continue
            
            matches.append(dict(
                path=path.replace('\\', '/'),
                keywords=m.groupdict(),
            ))
                
            # TODO: check if detected shot exists in Kitsu
        
        return matches
    
    def copy_source(self, src_path, dst_path):
        if os.path.splitext(src_path)[1] == '.mp4' and self.enable_mov_conversion.get():
            temp_mov = os.path.join(os.path.dirname(src_path), f'{next(tempfile._get_candidate_names())}.mov')
            ffmpeg_exe = self.root().project().admin.project_settings.ffmpeg_path.get()
            cmd = [ffmpeg_exe, '-i', src_path, '-qscale', '0', temp_mov]

            subprocess.run(' '.join(cmd))
            shutil.move(temp_mov, dst_path)
        else:
            shutil.copyfile(src_path, dst_path)


class FolderPresetItem(FilePresetItem):

    content_regex = flow.Param('.*')

    @staticmethod
    def validate_path(path):
        return os.path.isdir(path)
    
    def copy_source(self, src_path, dst_path):
        regex = self.content_regex.get()
        os.makedirs(dst_path, exist_ok=True)
        
        if regex == '.*':
            copytree_no_metadata(src_path, dst_path)
        else:
            for path in glob.iglob(src_path+'/**/*', recursive=True):
                relpath = path.replace('\\', '/').replace(src_path+'/', '')
                
                if re.match(regex, relpath) is None:
                    continue

                if os.path.isdir(path):
                    copytree_no_metadata(path, os.path.join(dst_path, relpath))
                else:
                    shutil.copyfile(path, os.path.join(dst_path, relpath))


class DeliveryFolderValue(flow.values.SessionValue):

    DEFAULT_EDITOR = 'choice'
    STRICT_CHOICES = False

    _settings = flow.Parent()

    def choices(self):
        root = self._settings.root_folder.get()
        dirs = []

        if root and os.path.isdir(root):
            for d in os.listdir(root):
                if os.path.isdir(os.path.join(root, d)):
                    dirs.append(d)
        
        return dirs

    def revert_to_default(self):
        dirs = self.choices()
        self.set(dirs[-1] if dirs else '')


class AddFilePreset(flow.Action):

    _map = flow.Parent()

    def needs_dialog(self):
        return False

    def run(self, button):
        self._map.add('p%04i' % len(self._map))
        self._map.touch()


class AddFolderPreset(flow.Action):

    _map = flow.Parent()

    def needs_dialog(self):
        return False

    def run(self, button):
        self._map.add('p%04i' % len(self._map), object_type=FolderPresetItem)
        self._map.touch()


class FilePresetMap(flow.Map):

    add_file_preset = flow.Child(AddFilePreset)
    add_folder_preset = flow.Child(AddFolderPreset)

    @classmethod
    def mapped_type(cls):
        return FilePresetItem
    
    def columns(self):
        return ['Name', 'Target']
    
    def _fill_row_cells(self, row, item):
        row['Name'] = item.display_name.get()
        row['Target'] = f'{item.target_task.get()}/{item.target.get()}'


class ImportSettings(flow.Object):

    ICON = ('icons.gui', 'settings')

    root_folder = MultiOSParam().watched()
    delivery_folder  = flow.Param(None, DeliveryFolderValue).watched()
    shot_name_regex = flow.Param('.*').watched()
    
    # Values used to format sequence and shot numbers detected in delivery folder
    sequence_prefix = flow.Param('sq')
    sequence_padding = flow.IntParam(3)
    shot_prefix = flow.Param('sh')
    shot_padding = flow.IntParam(3)
    
    file_presets = flow.Child(FilePresetMap)

    with flow.group('Preview'):
        preview_display_name = flow.Param('Preview')
        preview_regex = flow.Param().watched()
        preview_comment = flow.Param('').watched()
        target_task = flow.Param(dict)

    _action = flow.Parent()

    def child_value_changed(self, child_value):
        if child_value in (self.root_folder, self.delivery_folder, self.shot_name_regex):
            self._action.shot_sources.refresh()


class MatchSelectionValue(flow.values.SessionValue):

    DEFAULT_EDITOR = 'choice'
    STRICT_CHOICES = False

    _file_item = flow.Parent(2)

    def choices(self):
        return [
            f'{i} - {m["path"]}'
            for i, m in enumerate(self._file_item.matches.get())
        ]
    
    def revert_to_default(self):
        choices = self.choices()
        if choices:
            self.set(choices[0])


class SelectSourceFile(flow.Action):

    source_file = flow.SessionParam(None, MatchSelectionValue)

    _file_item = flow.Parent()
    _map = flow.Parent(2)

    def needs_dialog(self):
        self.source_file.revert_to_default()
        return True
    
    def get_buttons(self):
        return ['Confirm', 'Cancel']
    
    def run(self, button):
        if button == 'Cancel':
            return
        
        index, path = self.source_file.get().split(' - ')
        match = self._file_item.matches.get()[int(index)]
        self._file_item.path.set(path)
        self._file_item.keywords.set(match['keywords'])
        self._map.touch()


class CheckBlenderFileSoundtrack(GenericRunAction):
    '''
    Ensures that the Blender scene has a valid soundtrack.

    It relies on the check_soundtrack.py script run in
    background on the Blender file.
    '''
    _source_file = flow.Parent()

    def runner_name_and_tags(self):
        return 'Blender', []
    
    def allow_context(self, context):
        path = self._source_file.path.get()
        return path and os.path.splitext(path)[1] == '.blend'
    
    def target_file_extension(self):
        return 'blend'
    
    def get_run_label(self):
        return f'Check sountrack: {os.path.basename(os.path.splitext(self._source_file.path.get())[0])}'
    
    def extra_argv(self):
        args = [
            self._source_file.path.get(),
            '-b',
            '--python', resources.get('scripts.blender', 'check_soundtrack.py')
        ]
        return args
    
    def run(self, button):
        if os.path.splitext(self._source_file.path.get())[1] != '.blend':
            return
        
        return super(CheckBlenderFileSoundtrack, self).run(button)


class SourceFileItem(flow.SessionObject):

    matches = flow.Param()
    path = flow.Param()
    keywords = flow.Param(dict)

    select_source = flow.Child(SelectSourceFile)
    check_soundtrack = flow.Child(CheckBlenderFileSoundtrack)


class SourceFileMap(flow.DynamicMap):

    _shot_item = flow.Parent(2)

    @classmethod
    def mapped_type(cls):
        return SourceFileItem
    
    def mapped_names(self, page_num=0, page_size=None):
        return self._shot_item.file_matches.get().keys()
    
    def columns(self):
        return ['Source file', 'Path']
    
    def refresh(self):
        self._mng.children.clear()
        self.touch()
    
    def _configure_child(self, child):
        matches = self._shot_item.file_matches.get()[child.name()]
        child.matches.set(matches)
        if len(matches) == 1:
            child.path.set(matches[0]['path'])
            child.keywords.set(matches[0]['keywords'])
    
    def _fill_row_cells(self, row, item):
        row['Source file'] = self._shot_item.file_match_names.get()[item.name()]
        row['Path'] = item.path.get()
    
    def _fill_row_style(self, style, item, row):
        style['activate_oid'] = item.select_source.oid()


class SelectSourceFiles(flow.Action):

    source_files = flow.Child(SourceFileMap).ui(expanded=True)

    _shot_item = flow.Parent()
    _map = flow.Parent(2)

    def get_buttons(self):
        return ['Confirm', 'Cancel']
    
    def run(self, button):
        if button == 'Cancel':
            self.source_files.refresh()
        else:
            self._shot_item.selected.set(None not in [sf.path.get() for sf in self.source_files.mapped_items()])
            self._map.touch()


class OpenSourceFolder(flow.Action):

    ICON = ('icons.gui', 'open-folder')

    _shot_item = flow.Parent()

    def needs_dialog(self):
        return False
    
    def run(self, button):
        folder = self._shot_item.delivery_folder.get()

        if os.path.isdir(folder):
            if platform.system() == 'Windows':
                os.startfile(folder)
            elif platform.system() == 'Linux':
                subprocess.Popen(f'xdg-open "{folder}"')
            else:
                subprocess.Popen(f'open "{folder}"')


class ToggleSourceItemSelected(flow.Action):

    _item = flow.Parent()
    _map = flow.Parent(2)

    def needs_dialog(self):
        return False
    
    def allow_context(self, context):
        return False
    
    def run(self, button):
        self._item.selected.set(not self._item.selected.get())
        self._map.touch()


class ShotSourceItem(flow.SessionObject):

    ICON = ('icons.flow', 'shot')

    delivery_folder = flow.Param()
    shot_data = flow.DictParam(dict)
    target_sequence_name = flow.Param()
    target_shot_name = flow.Param()
    file_matches = flow.Param(dict)
    file_match_names = flow.Param(dict)
    preview_path = flow.Param()
    preview_comment = flow.Param()
    exists_kitsu = flow.Param(False).ui(editor='bool')
    selected = flow.Param(False).ui(editor='bool')
    exists = flow.Param(False).ui(editor='bool')

    open_folder = flow.Child(OpenSourceFolder).ui(label='Open')
    toggle_selected = flow.Child(ToggleSourceItemSelected)
    select_sources = flow.Child(SelectSourceFiles)

    _action = flow.Parent(2)
    _film = flow.Parent(3)

    def ensure_created_on_kitsu(self):
        if not self.exists_kitsu.get():
            self.root().project().kitsu_api().create_shot(self.target_sequence_name.get(), self.target_shot_name.get())

    def find_matches(self):
        matches = {}
        for fp in self._action.settings.file_presets.mapped_items():
            matches[fp.name()] = fp.find_matches(self.delivery_folder.get(), **(self.shot_data.get() or {}))
        
        return matches
    
    def find_preview(self):
        return self._action.find_preview(self.delivery_folder.get(), self.file_matches.get(), **(self.shot_data.get() or {}))
    
    def shot_exists(self):
        return all([
            not f.check_exists.get() or self._target_exists(f.target_task.get(), f.target.get())
            for f in self._action.settings.file_presets.mapped_items()
        ])
    
    def import_files(self):
        print(f'{self.target_sequence_name.get()} {self.target_shot_name.get()} :: import files')
        file_presets = self._action.settings.file_presets

        for sf in self.select_sources.source_files.mapped_items():
            src_path = sf.path.get()
            if src_path is not None:
                preset = file_presets[sf.name()]
                target = self._ensure_target(preset, os.path.isdir(src_path))
                if os.path.splitext(src_path)[1] == '.blend':
                    self._fill_history(sf, target)
                self._import_file(src_path, preset, target, sf.keywords.get())
    
    def upload_preview(self):
        path = self.preview_path.get()
        comment = self.preview_comment.get()
        
        if path is not None:
            if os.path.splitext(path)[1] == '.mp4':
                mov = os.path.join(f'{os.path.splitext(path)[0]}.mov')
                ffmpeg_exe = self.root().project().admin.project_settings.ffmpeg_path.get()
                cmd = [ffmpeg_exe, '-i', path, '-qscale', '0', mov]

                subprocess.run(' '.join(cmd))
                path = mov
            
            print(f'{self.target_sequence_name.get()} {self.target_shot_name.get()} :: upload preview: {path}')
            self._action.upload_preview(path, self.target_sequence_name.get(), self.target_shot_name.get(), comment)
            self._action.set_frames_count(path, self.target_sequence_name.get(), self.target_shot_name.get())
    
    def _check_file(self, source_file):
        '''
        Checks a source file before import.
        '''
        if os.path.splitext(source_file.path.get())[1] == '.blend':
            print(f'{self.target_sequence_name.get()} {self.target_shot_name.get()} :: {source_file.path.get()} - check soundtrack')
            res = source_file.check_soundtrack.run(None)
            rid = res['runner_id']

            runner_info = self.root().session().cmds.SubprocessManager.get_runner_info(rid)

            while runner_info['is_running']:
                time.sleep(1)
                runner_info = self.root().session().cmds.SubprocessManager.get_runner_info(rid)
    
    def _fill_history(self, source_file, target_file):
        target_version = source_file.keywords.get().get('version')
        if target_version is None or not target_version.isdigit():
            return
        
        print(f'{self.target_sequence_name.get()} {self.target_shot_name.get()} :: {source_file.path.get()} - create missing revisions')
        for i in range(1, int(target_version)):
            if not target_file.has_revision(f'v{i:03}'):
                r = target_file.add_revision(f'v{i:03}', comment='empty revision')
                r.set_sync_status('NotAvailable')
    
    def _ensure_target(self, preset, is_dir):
        sequence_name = self.target_sequence_name.get()
        shot_name = self.target_shot_name.get()
        dst_task_name = preset.target_task.get()
        dst_name = preset.target.get()
        
        if not self._film.sequences.has_mapped_name(sequence_name):
            sequence = self._film.sequences.add(sequence_name)
        else:
            sequence = self._film.sequences[sequence_name]
        
        if not sequence.shots.has_mapped_name(shot_name):
            shot = sequence.shots.add(shot_name)
        else:
            shot = sequence.shots[shot_name]
        
        task = shot.tasks[dst_task_name]
        dst_mapped_name = dst_name.replace('.', '_')

        if not task.files.has_mapped_name(dst_mapped_name):
            default_task = self.root().project().get_task_manager().default_tasks[dst_task_name]
            path_format = None

            if default_task.files.has_mapped_name(dst_mapped_name):
                path_format = default_task.files[dst_mapped_name].path_format.get()
            
            if is_dir:
                target = task.files.add_folder(dst_mapped_name, display_name=dst_name, tracked=True, default_path_format=path_format)
            else:
                name, ext = dst_name.split('.')
                target = task.files.add_file(name, ext, display_name=dst_name, tracked=True, default_path_format=path_format)
        else:
            target = task.files[dst_mapped_name]
        
        return target
    
    def _import_file(self, src_path, preset, target, comment_kwords):
        comment_kwords['delivery_date'] = self._action.settings.delivery_folder.get()
        comment = preset.comment.get()
        comment = comment.format_map(SafeDict(**comment_kwords))
        revision = target.add_revision(comment=comment)
        revision_path = revision.get_path()
        os.makedirs(os.path.dirname(revision_path), exist_ok=True)

        target.make_current(revision)

        print(f'  {preset.display_name.get()}: {src_path} -> {revision_path}')
        preset.copy_source(src_path, revision_path)
    
    def _target_exists(self, task_name, file_name):
        target_file = None
        sq_name = self.target_sequence_name.get()
        
        if self._film.sequences.has_mapped_name(sq_name):
            sq = self._film.sequences[sq_name]
            sh_name = self.target_shot_name.get()
            if sq.shots.has_mapped_name(sh_name):
                sh = sq.shots[sh_name]
                if sh.tasks.has_mapped_name(task_name):
                    task = sh.tasks[task_name]
                    file_name = file_name.replace('.', '_')
                    if task.files.has_mapped_name(file_name):
                        target_file = task.files[file_name]
        
        return (
            target_file is not None
            and target_file.get_head_revision() is not None
        )


class RefreshShotSources(flow.Action):

    _map = flow.Parent()

    def needs_dialog(self):
        return False
    
    def allow_context(self, context):
        return context and context.endswith('.inline')
    
    def run(self, button):
        self._map.refresh()


class ShotSourceMap(flow.DynamicMap):

    ICON = ('icons.gui', 'folder-closed-black-shape')
    
    refresh_action = flow.Child(RefreshShotSources).ui(label='Refresh')

    _action = flow.Parent()

    @classmethod
    def mapped_type(cls):
        return ShotSourceItem
    
    def __init__(self, parent, name):
        super(ShotSourceMap, self).__init__(parent, name)
        self._cache = None
    
    def mapped_names(self, page_num=0, page_size=None):
        if self._cache is None:
            self._cache = {}

            self._action.settings.root_folder.set_watched(False)
            root_folder = self._action.settings.root_folder.get()
            self._action.settings.root_folder.set_watched(True)

            if root_folder and os.path.isdir(root_folder):
                in_delivery_folder = os.path.join(root_folder, self._action.settings.delivery_folder.get())
                
                if os.path.isdir(in_delivery_folder):
                    regex = self._action.settings.shot_name_regex.get()
                    
                    for path in os.listdir(in_delivery_folder):
                        if not os.path.isdir(os.path.join(os.path.join(in_delivery_folder, path))):
                            continue
                        
                        m = re.match(regex, path)

                        if m is None:
                            continue

                        # Ensure shot and sequence numbers/names are provided
                        shot_number = m.groupdict().get('shot_number')
                        sequence_number = m.groupdict().get('sequence_number')
                        shot_name = m.groupdict().get('shot_name')
                        sequence_name = m.groupdict().get('sequence_name')

                        if (shot_number is None and shot_name is None) or (sequence_number is None and sequence_name is None):
                            continue

                        self._cache[f'ss_{(sequence_name or sequence_number)}_{(shot_name or shot_number)}'] = dict(
                            delivery_folder=os.path.join(in_delivery_folder, path).replace('\\', '/'),
                            shot_data=m.groupdict(),
                        )
        
        return self._cache.keys()
    
    def columns(self):
        return (
              ['Shot']
            + [fp.display_name.get() for fp in self._action.settings.file_presets.mapped_items()]
            + [self._action.settings.preview_display_name.get()]
        )
    
    def refresh(self):
        self._mng.children.clear()
        self._cache = None
        self.touch()
    
    def summary(self):
        if any([sh.exists.get() for sh in self.mapped_items()]):
            return '⚠️ Some shots have already been imported'
    
    def _configure_child(self, child):
        child.delivery_folder.set(self._cache[child.name()]['delivery_folder'])
        shot_data = self._cache[child.name()]['shot_data']
        child.shot_data.set(shot_data)
        # Format names to fit with project naming conventions
        sequence_name = self._action.get_sequence_name(shot_data)
        shot_name = self._action.get_shot_name(shot_data)
        child.target_sequence_name.set(sequence_name)
        child.target_shot_name.set(shot_name)
        child.file_matches.set(child.find_matches())
        child.file_match_names.set({
            fp.name(): fp.display_name.get()
            for fp in self._action.settings.file_presets.mapped_items()
        })
        
        preview = child.find_preview()
        child.preview_path.set(preview['path'])
        child.preview_comment.set(preview['comment'])
        child.exists_kitsu.set(self._action.kitsu_shot_exists(sequence_name, shot_name))
        child.exists.set(child.shot_exists())
    
    def _fill_row_cells(self, row, item):
        row['Shot'] = item.target_sequence_name.get() + ' ' + item.target_shot_name.get()
        if item.exists.get():
            row['Shot'] += '⚠️'

        for fp_name in item.file_matches.get():
            fp = self._action.settings.file_presets[fp_name]
            sf = item.select_sources.source_files[fp_name]
            match_count = len(sf.matches.get())
            
            row[fp.display_name.get()] = (
                sf.path.get() is not None and '✔️'
                or match_count == 0 and '❌'
                or f'⚠️ {match_count} found'
            )
        
        row[self._action.settings.preview_display_name.get()] = (
            item.preview_path.get() is not None and '✔️' or '❌'
        )
    
    def _fill_row_style(self, style, item, row):
        style['icon'] = item.selected.get() and ('icons.gui', 'check') or ('icons.gui', 'check-box-empty')
        style['activate_oid'] = item.select_sources.oid()
        style['Shot_activate_oid'] = item.toggle_selected.oid()


class ImportShotFiles(flow.Action):

    ICON = ('icons.gui', 'file')

    settings = flow.Child(ImportSettings).ui(expanded=False)
    shot_sources = flow.Child(ShotSourceMap).ui(expanded=True)
    select_all = flow.SessionParam().ui(editor='bool').watched()

    def needs_dialog(self):
        ffmpeg_exe = self.root().project().admin.project_settings.ffmpeg_path.get()
        
        if not ffmpeg_exe or not os.path.exists(ffmpeg_exe):
            self.message.set(
                '<h2>Import shot files</h2>'
                '<font color=#D5000D>A valid path to FFmpeg needs '
                'to be configured in the project settings.</font>'
            )
        else:
            self.message.set('<h2>Import shot files</h2>')

        self.settings.delivery_folder.set_watched(False)
        self.settings.delivery_folder.revert_to_default()
        self.settings.delivery_folder.set_watched(True)
        self.shot_sources.refresh()
        return True

    def get_buttons(self):
        ffmpeg_exe = self.root().project().admin.project_settings.ffmpeg_path.get()

        if not ffmpeg_exe or not os.path.isfile(ffmpeg_exe):
            return ['Cancel']
        else:
            return ['Import', 'Cancel']
    
    def find_preview(self, root_folder, files_matches, **keywords):
        '''
        Returns the first file in the given folder with the name
        matching the `preview_regex` regular expression.
        Returns None if no file is found.
        '''
        regex = self.settings.preview_regex.get()

        if not root_folder or not os.path.exists(root_folder) or regex is None:
            return dict(path=None, comment=None)
        
        regex = safe_format(regex, keywords)
        _match = dict(path=None, comment=None)
        
        for path in glob.iglob(root_folder+'/**/*', recursive=True):
            if not os.path.isfile(path):
                continue

            relpath = path.replace('\\', '/').replace(root_folder.replace('\\', '/') + '/', '')
            m = re.match(regex, relpath)

            if m is None:
                continue

            comment_kwords = m.groupdict()
            comment_kwords['delivery_date'] = self.settings.delivery_folder.get()

            # Use blend version on preview (kitsu) comment
            if files_matches['p0000']:
                comment_kwords['blend_version'] = files_matches['p0000'][0]['keywords']['version']
            else:
                comment_kwords['blend_version'] = comment_kwords['version']
            
            comment = self.settings.preview_comment.get().format_map(SafeDict(**comment_kwords))
            
            _match = dict(
                path=path.replace('\\', '/'),
                comment=comment,
            )
            break
                
            # TODO: check if detected shot exists in Kitsu
        
        return _match
    
    def upload_preview(self, preview_path, sequence_name, shot_name, comment):
        target_task = self.settings.target_task.get()
        kitsu = self.root().project().kitsu_api()
        kitsu.upload_shot_preview(
            sequence_name,
            shot_name,
            target_task['type'],
            target_task['status'],
            preview_path,
            comment)

    def set_frames_count(self, preview_path, sequence_name, shot_name):
        shot_data = self.root().project().kitsu_api().get_shot_data(shot_name, sequence_name)
        frames = shot_data['nb_frames']

        if frames is None:
            site_env = self.root().project().get_current_site().site_environment
            if site_env.has_mapped_name('FFPROBE_EXEC_PATH'):
                exec_path = site_env['FFPROBE_EXEC_PATH'].value.get()

                check_frames = subprocess.check_output(
                    f'{exec_path} -v quiet -show_streams -select_streams v:0 -of json "{preview_path}"',
                    shell=True).decode()

                fields = json.loads(check_frames)['streams'][0]
                shot_data['nb_frames'] = int(fields['nb_frames'])

                return gazu.shot.update_shot(shot_data)
    
    def kitsu_shot_exists(self, sequence_name, shot_name):
        shot = self.root().project().kitsu_api().get_shot_data(shot_name, sequence_name)
        return shot is not None
    
    def get_sequence_name(self, shot_data):
        sequence_name = shot_data.get('sequence_name')

        if sequence_name is not None:
            return sequence_name
        
        # Update padding of all numeric parts of the name
        sequence_name = ''.join([
            s.zfill(self.settings.sequence_padding.get()) if s.isdigit() else s
            for s in re.findall('\d+|[A-Za-z]+', shot_data['sequence_number'])
        ])
        return self.settings.sequence_prefix.get() + sequence_name

    def get_shot_name(self, shot_data):
        shot_name = shot_data.get('shot_name')

        if shot_name is not None:
            return shot_name
        
        # Same as for sequence
        shot_name = ''.join([
            s.zfill(self.settings.shot_padding.get()) if s.isdigit() else s
            for s in re.findall('\d+|[A-Za-z]+', shot_data['shot_number'])
        ])
        return self.settings.shot_prefix.get() + shot_name
    
    def child_value_changed(self, child_value):
        if child_value is self.select_all:
            for item in self.shot_sources.mapped_items():
                item.selected.set(self.select_all.get())
            
            self.shot_sources.touch()

    def run(self, button):
        if button == 'Cancel':
            return
        
        for ss in self.shot_sources.mapped_items():
            if ss.selected.get():
                ss.ensure_created_on_kitsu()
                ss.import_files()   # Import files
                ss.upload_preview() # Upload rough to Kitsu


def import_shot_files(parent):
    if re.match('^/[^/]+/films/[^/]+(_test)?$', parent.oid()):
        r = flow.Child(ImportShotFiles).ui(dialog_min_size=(700, 450))
        r.name = 'import_shot_files'
        r.index = None
        return r


def install_extensions(session): 
    return {
        "march": [
            import_shot_files
        ],
    }

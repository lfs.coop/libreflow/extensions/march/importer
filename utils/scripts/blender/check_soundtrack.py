import bpy
import os
import re
import glob


def find_movie(regex):
    path = None
    refs_dir = os.path.normpath(os.path.dirname(bpy.data.filepath)+'/../IMAGES')

    for f in glob.glob(refs_dir+'/*.mov'):
        basename = os.path.basename(f)
        if re.fullmatch(regex, basename) is not None:
            path = os.path.join('//..', 'IMAGES', basename)
            break
    
    return path

def find_movie_by_name(filename):
    path = None
    refs_dir = os.path.normpath(os.path.dirname(bpy.data.filepath)+'/../IMAGES')

    for f in glob.glob(refs_dir+'/*.mov'):
        basename = os.path.basename(f)
        if basename == filename:
            path = os.path.join('//..', 'IMAGES', basename)
            break
    
    return path


scene = bpy.context.scene

if not scene.sequence_editor.sequences:
    mov_path = find_movie(r'sc\d{3}\D?_sh\d{4}_\D+_\d{3}_\D+_\d{6}.mov')
    if mov_path:
        mov_name = os.path.basename(mov_path)
        strip = scene.sequence_editor.sequences.new_sound(mov_name, mov_path, 1, 1)
        print(f"\nINFO    :: Import :: {os.path.basename(bpy.data.filepath)}: "
              f"soundtrack imported: {mov_name}")
    else:
        print(f"\nWARNING :: Import :: {os.path.basename(bpy.data.filepath)}: "
               "no movie found, this scene won't have any soundtrack")
else:
    for strip in scene.sequence_editor.sequences:
        mov_name = os.path.basename(strip.sound.filepath)
        mov_path = find_movie_by_name(mov_name)

        if mov_path:
            if mov_path != strip.sound.filepath:
                strip.sound.filepath = mov_path
                print(f"\nINFO    :: Import :: {os.path.basename(bpy.data.filepath)}: "
                    f"soundtrack updated: {mov_name}")
        else:
            print(f"\nWARNING :: Import :: {os.path.basename(bpy.data.filepath)}: "
                  f"movie {mov_name} not found")

bpy.ops.wm.save_mainfile()
bpy.ops.wm.quit_blender()

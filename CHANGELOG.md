# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)[^1].

<!---
Types of changes

- Added for new features.
- Changed for changes in existing functionality.
- Deprecated for soon-to-be removed features.
- Removed for now removed features.
- Fixed for any bug fixes.
- Security in case of vulnerabilities.

-->

## [Unreleased]

### Added

* An action to import source files and folders located in a given root folder into the shot tasks. Each possible target defines a regular expression, which the path of a source inside the root folder must match to be imported. If multiple sources match a single target, these will be imported as individual revisions of this target, in the order they are found in the root folder.
The action also allows to search for a video in the root folder to send as a preview of a given task in Kitsu.